# README #

This is a openWRT package for i2s audio on the onion omega, a ar9331 based development board.
It is based on the package by Franz Flasch at [https://github.com/franzflasch/ar9331-i2s-alsa](https://github.com/franzflasch/ar9331-i2s-alsa)

It aims to support the pcm5100 audio DAC by Texas Instruments.