/*
 *  pcm5100a.c
 *
 *  Created on: 12-19-15
 *  Author: Samuel Ellicott (nebk2010@gmail.com)
 *  Modified from wm8727.c
 *
 *  Copyright (C) 2015 Samuel Ellicott
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 */

#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/ac97_codec.h>
#include <sound/initval.h>
#include <sound/soc.h>

//TODO change constants and add i2c setup

static const struct snd_soc_dapm_widget pcm510x_dapm_widgets[] = {
    SND_SOC_DAPM_OUTPUT("VOUTL"),
    SND_SOC_DAPM_OUTPUT("VOUTR"),
};

static const struct snd_soc_dapm_route pcm510x_dapm_routes[] = {
    { "VOUTL", NULL, "Playback" },
    { "VOUTR", NULL, "Playback" },
};

/*
 * Note this is a simple chip with no configuration interface, sample rate is
 * determined automatically by examining the Master clock and Bit clock ratios
 */
#define PCM510X_RATES  (SNDRV_PCM_RATE_32000 | SNDRV_PCM_RATE_44100 |\
                        SNDRV_PCM_RATE_48000 | SNDRV_PCM_RATE_96000 |\
                        SNDRV_PCM_RATE_192000)


static struct snd_soc_dai_driver pcm510x_dai = {
    .name = "pcm510xA-DAC",
    .playback = {
        .stream_name = "Playback",
        .channels_min = 2,
        .channels_max = 2,
        .rates = PCM510X_RATES,
        .formats = SNDRV_PCM_FMTBIT_S16_LE | SNDRV_PCM_FMTBIT_S32_LE,
    },
};

static struct snd_soc_codec_driver soc_codec_dev_pcm510x = {
    .dapm_widgets = pcm510x_dapm_widgets,
    .num_dapm_widgets = ARRAY_SIZE(pcm510x_dapm_widgets),
    .dapm_routes = pcm510x_dapm_routes,
    .num_dapm_routes = ARRAY_SIZE(pcm510x_dapm_routes),
};

static int pcm510x_probe(struct platform_device *pdev)
{
    return snd_soc_register_codec(&pdev->dev,
           &soc_codec_dev_pcm510x, &pcm510x_dai, 1);
}

static int pcm510x_remove(struct platform_device *pdev)
{
    snd_soc_unregister_codec(&pdev->dev);
    return 0;
}

static struct platform_driver pcm510x_codec_driver = {
    .driver = {
        .name = "pcm510x",
        .owner = THIS_MODULE,
    },

    .probe = pcm510x_probe,
    .remove = pcm510x_remove,
};

module_platform_driver(pcm510x_codec_driver);

MODULE_DESCRIPTION("ASoC pcm501xA driver");
MODULE_AUTHOR("Samuel Ellicott");
MODULE_LICENSE("GPL");
